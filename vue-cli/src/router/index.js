import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home/index.vue'
import Cart from '../views/Cart/index.vue'
import Friends from '../views/Friends/index.vue'
import Search from '../views/Search/index.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    // name: 'Home',
    component: Home,
    meta: {
      title: 'home',
      active: 0
    }
  },
  {
    path: '/friends',
    // name: 'filters',
    component: Friends,
    meta: {
      title: 'friends',
      active: 1
    }
  },
  {
    path: '/cart',
    // name: 'cart',
    component: Cart,
    meta: {
      title: 'cart',
      active: 2
    }
  },
  {
    path: '/search',
    compoment: Search,
    meta: {
      title: 'search',
      active: 3
    }
  }
]

const router = new VueRouter({
  routes
})

export default router
